import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

let apiKey = "8b3bd84c83e04c84b3ac755e94c51213";

export default new Vuex.Store({
  state: {
    latestNews: [],
    aj: [],
    bbc: [],
    sports: [],
    loading: false
  },
  getters: {
    loadLatestNews(state) {
      return state.latestNews;
    },
    loadalJazeera(state) {
      return state.aj;
    },
    loadbbc(state) {
      return state.bbc;
    },
    loadSports(state) {
      return state.sports;
    },
    loading(state) {
      return state.loading;
    }
  },
  mutations: {
    loadNews(state, payload) {
      state.latestNews = payload;
    },
    loadalJazeera(state, payload) {
      state.aj = payload;
    },
    loadbbc(state, payload) {
      state.bbc = payload;
    },
    loadSports(state, payload) {
      state.sports = payload;
    },
    loading(state, payload) {
      state.loading = payload;
    }
  },
  actions: {
    loadNews({ commit }) {
      let url = `https://newsapi.org/v2/top-headlines?country=ng&apiKey=${apiKey}`;

      let req = new Request(url);

      commit("loading", true);
      fetch(req)
        .then(function(response) {
          commit("loading", true);
          return response.json();
        })
        .then(function(payload) {
          let latestNews = payload.articles;
          console.log(latestNews);
          commit("loading", false);
          commit("loadNews", latestNews);
        });
    },
    loadalJazeera({ commit }) {
      var url = `https://newsapi.org/v2/top-headlines?sources=al-jazeera-english&apiKey=${apiKey}`;

      let req = new Request(url);

      commit("loading", true);
      fetch(req)
        .then(function(response) {
          commit("loading", true);
          return response.json();
        })
        .then(function(payload) {
          let alJazeera = payload.articles;
          console.log(alJazeera);
          commit("loading", false);
          commit("loadalJazeera", alJazeera);
        });
    },
    loadbbc({ commit }) {
      var url = `https://newsapi.org/v2/top-headlines?sources=bbc-news&apiKey=${apiKey}`;

      let req = new Request(url);

      commit("loading", true);
      fetch(req)
        .then(function(response) {
          commit("loading", true);
          return response.json();
        })
        .then(function(payload) {
          let bbc = payload.articles;
          console.log(bbc);
          commit("loading", false);
          commit("loadbbc", bbc);
        });
    },
    loadSports({ commit }) {
      var url = `https://newsapi.org/v2/everything?q=sports&sortBy=popularity&apiKey=${apiKey}`;

      let req = new Request(url);

      commit("loading", true);
      fetch(req)
        .then(function(response) {
          commit("loading", true);
          return response.json();
        })
        .then(function(payload) {
          let sports = payload.articles;
          console.log(sports);
          commit("loading", false);
          commit("loadSports", sports);
        });
    }
  }
});
